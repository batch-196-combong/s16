// Arithmetic Operations

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Addition Operator: " + sum);

let difference = y - x;
console.log("Subtraction Operator: " + difference);

let product = x * y;
console.log("Multiplication Operator: " + product);

let quotient = x / y;
console.log("Division Operator: " + quotient);

let remainder = y % x;
console.log("Modulo Operator: " + remainder)

// Assignment Operator
	// Basic Assignment Operator (=)
let assignmentNumber = 8;

	// Addition Assignment Operator (+=)

assignmentNumber += 2;
console.log("Additional Assignment: " + assignmentNumber)

assignmentNumber += 2;
console.log("Additional Assignment: " + assignmentNumber)

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);


assignmentNumber -= 2;
console.log("Subtraction Assignment: " + assignmentNumber)

assignmentNumber *= 2;
console.log("Multiplication Assignment: " + assignmentNumber)

assignmentNumber /= 2;
console.log("Division Assignment: " + assignmentNumber)

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("MDAS: " + mdas)

let z = 1;

let increment = ++z;
console.log("Pre-Increment: " + increment);

console.log("Value of z: " + z);

increment = z++;
console.log("Post-Increment: " + increment);

console.log("Value of z: " + z);

// type coercion

let numA ='10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);

console.log(typeof coercion); //string

// Equality Operator (==)
console.log (1==1)
console.log (1==2)
console.log (1=='1')
console.log (false ==0)
console.log ('johny' == 'johny')
console.log ('johny' == 'Johny')

// Inequality Operator (!=)
//opposite of equality
console.log (1!=1)
console.log (1!=2)
console.log (1!='1')


// Strict Equality Operator (===)
// data type must be the same
console.log (1===1)
console.log (1===2)
console.log (1==='1')

// Strict Inequality Operator (!==)
// data type must be the same as well
console.log (1!==1)
console.log (1!==2)
console.log (1!=='1')

// relational Operator

let a = 50;
let b = 65;

// GT
let isGreaterThan = a > b;
console.log(isGreaterThan)

// LT
let isLessThan = a < b;
console.log(isLessThan)

// GTE
let isGTE = a >= b;
console.log(isGTE)

// GT
let isLTE = a <= b;
console.log(isLTE)

let numStr = '30'
console.log(a > numStr)

let str = 'twenty';
console.log(b >= str);

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log (authorization1) 

let authorization2 = isLegalAge && isRegistered;

let authorization3 = isAdmin && isLegalAge; 
console.log (authorization3)

let random = isAdmin && isLegalAge
console.log(random);

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4)

let authorization5 = isRegistered && isLegalAge &&
requiredLevel === 95;
console.log(authorization5)

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

// .length see how many characters a variable have.
let registration1 = userName.length > 8;

// OR Operator (||)

let userLevel = 100;
let userLevel2 = 65;
let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log (guildRequirement3);

let guildAdmin =  isAdmin || userLevel2 >=requiredLevel;
console.log (guildAdmin)

// Not Operator (!)

console.log (!isRegistered);

let opposite1 = !isAdmin;
console.log (opposite1)
